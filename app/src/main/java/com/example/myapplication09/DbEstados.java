package com.example.myapplication09;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import static android.content.ContentValues.TAG;

public class DbEstados extends SQLiteOpenHelper {
    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String COMMA_SEP = ",";
    private static final String SQL_CREATE_ESTADO = "CREATE TABLE " +
            DefinirTabla.Estado.TABLE_NAME + " ("+
            DefinirTabla.Estado._ID + " INTEGER PRIMARY KEY, "+
            DefinirTabla.Estado.COLUMN_NAME_NOMBRE + TEXT_TYPE +
            COMMA_SEP +
            DefinirTabla.Estado.COLUMN_NAME_ESTATUS + INTEGER_TYPE +
            ")";
    private static final String SQL_DELETE_ESTADO = "DROP TABLE IF EXISTS " + DefinirTabla.Estado.TABLE_NAME;
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "logistica.db";

    public DbEstados(Context context){
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
    }
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int
            newVersion){
        db.execSQL(SQL_DELETE_ESTADO);
        onCreate(db);
    }
    public void deleteEstado(int id)
    {
        String query =
                "Delete from " +
                        DefinirTabla.Estado.TABLE_NAME +
                        " WHERE " + DefinirTabla.Estado._ID + " = " + id;
        SQLiteDatabase db = this.getReadableDatabase();
        db.execSQL(query);

        Log.d(TAG, "deleteEstado: ENTRO Y FUNCIONO");
    }
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int
            newVersion){
        onUpgrade(db, oldVersion, newVersion);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ESTADO);
    }
}

