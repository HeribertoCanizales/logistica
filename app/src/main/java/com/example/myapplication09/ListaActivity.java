package com.example.myapplication09;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class ListaActivity extends ListActivity implements
        Response.Listener<JSONObject>,Response.ErrorListener{
    DbEstados dbEstados;
    ProcesosPHP phpP;
    Estado estado;
    private final Context context = this;
    private EstadoAdapter proyectoEstado;
    private RequestQueue request;
    private JsonObjectRequest jsonObjectRequest;
    Device device;
    ListView listView;
    private MyArrayAdapter adapter;
    private ArrayList<Estado> listaContactos;
    ArrayList<Estado> copyContactoss = new ArrayList<>();
    private DbEstados proyectoEstados = new DbEstados(this);
    private String serverip = "https://estadosdub.000webhostapp.com/WebService/";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);
        Button btnNuevo = (Button)findViewById(R.id.btnNuevo);
        proyectoEstado = new EstadoAdapter(this);
        proyectoEstado.openDatabase();
        phpP = new ProcesosPHP();
        phpP.setContext(ListaActivity.this);
        request = Volley.newRequestQueue(context);
        listaContactos = new ArrayList<Estado>();
        /*if (estados.isEmpty())
        {
            estados.add(new Estado(1, "Sinaloa", 0));
            estados.add(new Estado(2, "Estado De Mexico", 0));
            estados.add(new Estado(3, "Sonorar", 0));
            estados.add(new Estado(4, "Nuevo Leon", 1));
            estados.add(new Estado(5, "Jalisco", 1));
            estados.add(new Estado(6, "Durango", 1));
            estados.add(new Estado(7, "Baja Califronia Sur", 1));
            estados.add(new Estado(8, "Baja California Norte", 1));
            estados.add(new Estado(9, "Chihuahua", 1));
            estados.add(new Estado(10, "Nayarit", 1));


        }*/
        proyectoEstado.addEstados(this.getEstadosOnce());

      /*  MyArrayAdapter adapter = new MyArrayAdapter(this,
                R.layout.activity_estado, estados);
        setListAdapter(adapter);*/
//NUEVO
        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(Activity.RESULT_CANCELED);
                finish();
            }
        });
        consultarTodosWebService();
    }
    public void consultarTodosWebService(){
        String url = serverip + "consulta.php?idMovil=" + Device.getSecureId(this);
    Log.e("",""+url);
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
        request.add(jsonObjectRequest);
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onResponse(JSONObject response) {
        JSONArray json = response.optJSONArray("estados");
        try {
            for (int i = 0; i < json.length(); i++) {
                estado = new Estado();
                JSONObject jsonObject = null;
                jsonObject = json.getJSONObject(i);
                estado.set_ID(jsonObject.optInt("id"));
                estado.setNombre(jsonObject.optString("nombre"));
                estado.setIdMovil(jsonObject.optString("idMovil"));
                listaContactos.add(estado);
            }
            copyContactoss.addAll(listaContactos);
            MyArrayAdapter adapter = new MyArrayAdapter(context, R.layout.activity_estado, listaContactos);
            //setListAdapter(adapter);
            this.adapter = adapter;
            setListAdapter(adapter);

        }catch (JSONException e){
            e.printStackTrace();
        }
    }


    class MyArrayAdapter extends ArrayAdapter<Estado> {
        Context context;
        int textViewResourceId;
        ArrayList<Estado> objects;
        public MyArrayAdapter(Context context, int textViewResourceId,
                              ArrayList<Estado> objects){
            super(context, textViewResourceId, objects);
            this.context = context;
            this.textViewResourceId = textViewResourceId;
            this.objects = objects;
        }
        public View getView(final int position, View convertView, ViewGroup
                viewGroup){
            LayoutInflater layoutInflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(this.textViewResourceId, null);

            TextView lblNombre = (TextView)view.findViewById(R.id.lblNombreEstado);
            Button modificar = (Button)view.findViewById(R.id.btnModificar);
            Button borrar = (Button)view.findViewById(R.id.btnBorrar);
            lblNombre.setTextColor(Color.BLACK);
            lblNombre.setText(objects.get(position).getNombre());
            borrar.setOnClickListener(new View.OnClickListener() {
                @Override

                public void onClick(View v) {
                    proyectoEstado.openDatabase();
                    proyectoEstados.deleteEstado(objects.get(position).get_ID());
                    phpP.borrarEstadoWebService(objects.get(position).get_ID());
                    proyectoEstado.close();
                    objects.remove(position);

                    notifyDataSetChanged();
                }
            });
            modificar.setOnClickListener(new View.OnClickListener() {
                @Override

                public void onClick(View v) {
                    Bundle oBundle = new Bundle();

                    oBundle.putSerializable("estado", objects.get(position));

                    Intent i = new Intent();
                    i.putExtras(oBundle);
                    setResult(Activity.RESULT_OK, i);
                    finish();

                }
            });
            return view;
        }
    }


    private ArrayList<Estado> getEstadosOnce () {
        ArrayList<Estado> estados = new ArrayList<>();
        return estados;
    }
}
