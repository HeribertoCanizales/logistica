package com.example.myapplication09;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import java.io.Serializable;

public class Estado implements Serializable {
    private int _ID;
    private String nombre;
    private int estatus;
    private String idMovil;

    public Estado(){
        this._ID = 0;
        this.nombre = "";
        this.estatus = 1;
        this.idMovil="";

    }
    public Estado(int _ID, String nombre, int estatus, String idMovil){
        this._ID = _ID;
        this.nombre = nombre;
        this.estatus = estatus;
        this.idMovil =idMovil;
    }
    public int get_ID() {
        return _ID;
    }
    public void set_ID(int _ID) {
        this._ID = _ID;
    }
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public int getEstatus() {
        return estatus;
    }
    public void setEstatus(int Estatus) {
        this.estatus = estatus;
    }
    public String getIdMovil() { return idMovil; }
    public void setIdMovil(String idMovil) { this.idMovil = idMovil; }
}
