package com.example.myapplication09;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.ArrayList;

public class ProcesosPHP implements Response.Listener<JSONObject>,Response.ErrorListener {
    private RequestQueue request;
    private JsonObjectRequest jsonObjectRequest;
    private ArrayList<Estado> estados = new ArrayList<Estado>();
    private String serverip = "https://estadosdub.000webhostapp.com/WebService/";
    //Agregar el servidor
    // "https://cursosandroid2018.000webhostapp.com/WebService/";
    public void setContext(Context context){
        request = Volley.newRequestQueue(context);
    }
    public void insertarEstadoWebService(Estado e){
        Log.e("asd","asd");
        String url = serverip + "agregar.php?nombre="+e.getNombre()
                +"&idMovil="+e.getIdMovil();
        url = url.replace(" ","%20");
        Log.e("zxczx",url);
        jsonObjectRequest = new
                JsonObjectRequest(Request.Method.GET,url,null,this,this);
        request.add(jsonObjectRequest);
    }
    public void actualizaEstadosWebService(Estado e,int id){
        String url = serverip + "modificar.php?id="+id
                +"&nombre="+e.getNombre();
        url = url.replace(" ","%20");
        Log.e("zxczx",url);
        jsonObjectRequest = new
                JsonObjectRequest(Request.Method.GET,url,null,this,this);
        request.add(jsonObjectRequest);
    }
    public void borrarEstadoWebService(int id){
        String url = serverip + "desactivar.php?id="+id;
        jsonObjectRequest = new
                JsonObjectRequest(Request.Method.GET,url,null,this,this);
        request.add(jsonObjectRequest);
    }
    public void mostrarUnoWebService(int id){
        String url = serverip + "consulta.php?_ID="+id;
        jsonObjectRequest = new
                JsonObjectRequest(Request.Method.GET,url,null,this,this);
        request.add(jsonObjectRequest);
    }
    public void mostrarTodosWebService(){
        String url = serverip + "consulta.php?_ID=";
        jsonObjectRequest = new
                JsonObjectRequest(Request.Method.GET,url,null,this,this);
        request.add(jsonObjectRequest);
    }
    @Override
    public void onErrorResponse(VolleyError error) {
        Log.i("ERROR",error.toString());
    }
    @Override
    public void onResponse(JSONObject response) {
    }
}
