package com.example.myapplication09;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private EditText edtNombre;
    private Estado savedEstado;
    private int id;
    private int estatus;
    private Button btnGuardar;
    private Button btnListar;
    private Button btnLimpiar;
    private Integer Contenedor;
    private Button btnCerrrar;

    ProcesosPHP sqlPhp;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        String[] estados = getResources().getStringArray(R.array.estados);
        SharedPreferences prefs = getBaseContext().getSharedPreferences("vacio1", Context.MODE_PRIVATE);
        sqlPhp = new ProcesosPHP();
        sqlPhp.setContext(this);
        Contenedor = prefs.getInt("id",0);
        if(Contenedor==0){
            Contenedor=1;
            SharedPreferences prefs1 = getSharedPreferences("vacio1", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = prefs1.edit();
            editor.putInt("id", Contenedor);
            editor.commit();
            for(int x = 0; x <10;x++){
                EstadoAdapter source = new EstadoAdapter(MainActivity.this);
                source.openDatabase();
                Estado nEstado = new Estado();
                nEstado.setNombre(estados[x]);
                nEstado.setEstatus(1);
                source.insertEstado(nEstado);
                source.close();
            }
            if(isNetworkAvailable())
            {
                Toast.makeText(getApplicationContext(), "Conexion a internet Exitosa",Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(getApplicationContext(), "Sin coneccion a Internet",Toast.LENGTH_SHORT).show();
            }
        }

        edtNombre = (EditText) findViewById(R.id.edtNombre);
        btnGuardar = (Button) findViewById(R.id.btnGuardar);
        btnListar = (Button) findViewById(R.id.btnListar);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);

        btnCerrrar = (Button) findViewById(R.id.btnCerrar);


        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean completo = true;
                if (edtNombre.getText().toString().equals("")) {
                    edtNombre.setError("Introduce el nombre");
                    completo = false;
                }
                if (completo) {
                    EstadoAdapter source = new EstadoAdapter(MainActivity.this);
                    source.openDatabase();

                    Estado nEstado = new Estado();
                    nEstado.setNombre(edtNombre.getText().toString());
                    nEstado.setEstatus(1);
                    nEstado.setIdMovil(Device.getSecureId(MainActivity.this));

                    if (savedEstado == null) {
                        source.insertEstado(nEstado);
                        sqlPhp.insertarEstadoWebService(nEstado);

                        Toast.makeText(MainActivity.this,R.string.mensaje,

                                Toast.LENGTH_SHORT).show();
                        limpiar();

                    } else {
                        source.updateEstado(nEstado,id);
                        Toast.makeText(MainActivity.this, R.string.mensajeedit,

                                Toast.LENGTH_SHORT).show();
                        sqlPhp.actualizaEstadosWebService(nEstado,id);
                        limpiar();
                    }

                    source.close();

                }
            }
        });
        btnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this,
                        ListaActivity.class);
                startActivityForResult(i, 0);
            }
        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });
        btnCerrrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode,Intent data)
    {
        if (Activity.RESULT_OK == resultCode) {
            Estado estado = (Estado) data.getSerializableExtra("estado");
            savedEstado = estado;
            id = estado.get_ID();
            edtNombre.setText(estado.getNombre());
            estatus = estado.getEstatus();
        }else{
            limpiar();
        }
    }public void limpiar(){
        savedEstado = null;
        edtNombre.setText("");
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        return ni != null && ni.isConnected();
    }
}


